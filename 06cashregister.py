#Asks for values from 2 items. Theres a sale for buy one and the cheaper one half off.
item1 = float(input("Enter price of the first item:"))
item2 = float(input("Enter price of the second item:"))

basetotal = float(item1 + item2)

#This first part ensures no matter how you type in the values, it will always make the lower priced one half off.

if item1 < item2:
    itemhalfstep1 = float(item1/20)
    itemhalfstep2 = float(itemhalfstep1*10)
    itemhalfstep3 = float(item1-itemhalfstep2)
    discount = float(basetotal-itemhalfstep3)
    
if item2 < item1:
    itemhalfstep1 = float(item2/20)
    itemhalfstep2 = float(itemhalfstep1*10)
    itemhalfstep3 = float(item2-itemhalfstep2)
    discount = float(basetotal-itemhalfstep3)
    
if item1 == item2:
    itemhalfstep1 = float(item1/20)
    itemhalfstep2 = float(itemhalfstep1*10)
    itemhalfstep3 = float(item1-itemhalfstep2)
    discount = float(basetotal-itemhalfstep3)
    
#The following lines ask for card info. If they do have a card it will do 10% off. If not that 0.
    
card = input("Does customer have a club card? (Y/N):")

if card == 'Y' or 'y':
    card = 100
    discountstep1 = float(discount/card)
    discountstep2 = float(discountstep1*10)
    discountfinal = float(discount-discountstep2)
       
if card == 'N' or 'n':
    discountfinal = discount
    
#Tax Calculations. (Ex. 8.25)        
taxstep1 = float(input("Enter tax rate, e.g. 5.5 for 5.5% tax:"))

taxstep2 = float(taxstep1/100)

taxstep3 = float(taxstep2*discountfinal)

taxstep4 = float(discountfinal+taxstep3)

taxstep5 = round(taxstep4,2)


#Prints total

print("Base price = {:.2f}".format(basetotal))

print("Price after discounts = {:.2f}".format(discountfinal))

print(f"Total price = {taxstep5}")