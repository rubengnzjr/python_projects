weight = float(input ("Please enter weight in pounds:"))
height = float(input ("Please enter height in inches:"))

weightkilo = weight*0.453592
heightmeter = height*0.0254

bmi = weightkilo/heightmeter**2

def truncate_float(value, digits_after_point=2):
    pow_10 = 10 ** digits_after_point
    return (float(int(value * pow_10))) / pow_10

bmi2 = truncate_float (bmi,10)

print("BMI is:",bmi2)