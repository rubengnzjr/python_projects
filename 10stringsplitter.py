#This programs splits sentences into sections
txt = input("Enter an odd length string: ")

#first half
first_half = txt[:len(txt)//2]
#second half
second_half = txt[len(txt)//2+1:]
#extracts middle character
def middle_char(txt):
   return txt[(len(txt)-1)//2:(len(txt)+2)//2]

mid_letter = (middle_char(txt))

print("Middle character:",mid_letter)
print("First half:", first_half)
print("Second half:",second_half)