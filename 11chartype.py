#Asks input from user
sym = input("Enter a character:")

#Step 1 checks if your a number. If false Step 2.
if sym.isdigit() == True:
    print (sym,"is a digit.")

#Step 2 check if its upper case. If false Step 3.   
elif sym.isupper() == True:
    print(sym,"is an upper case letter.")
    
#Step 3 check if its lower case. If false step 4.
elif sym.islower() == True:
    print(sym,"is a lower case letter.")
        
#Step 4 any other chaacter print a message.
else:
    print(sym,"is a non-alphanumeric character.")